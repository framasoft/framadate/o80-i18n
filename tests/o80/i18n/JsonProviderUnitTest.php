<?php

namespace o80\i18n;

use o80\I18NTestCase;

class JsonProviderUnitTest extends I18NTestCase
{
    /**
     * @var JsonProvider
     */
    private $jsonProvider;

    protected function setUp(): void
    {
        $this->jsonProvider = new JsonProvider();
        $this->jsonProvider->setLangsPath($this->getTestResourcePath('langs'));
    }

    /**
     * @test
     * 'en' will match to 'en'
     */
    public function shouldLoadExactFileTranslation_en(): void
    {
        // given

        // when
        $dict = $this->jsonProvider->load(array('en', ''));
        $loadedLang = $this->jsonProvider->getLoadedLang();

        // then
        $this->assertNotNull($dict);
        $this->assertCount(2, $dict);
        $this->assertEquals('en Hello World!', $dict['Some']['HELLOWORLD']);
        $this->assertEquals('en', $loadedLang);
    }

    /**
     * @test
     * 'en_GB' will match to 'en'
     */
    public function shouldLoadMatchingFileTranslation_enGB(): void
    {
        // given

        // when
        $dict = $this->jsonProvider->load(array('en_GB', ''));
        $loadedLang = $this->jsonProvider->getLoadedLang();

        // then
        $this->assertNotNull($dict);
        $this->assertCount(2, $dict);
        $this->assertEquals('en Hello World!', $dict['Some']['HELLOWORLD']);
        $this->assertEquals('en', $loadedLang);
    }

    /**
     * @test
     * 'en_US' will match to 'en_US' instead of 'en'
     */
    public function shouldLoadExactFileTranslation_en_US(): void
    {
        // given

        // when
        $dict = $this->jsonProvider->load(array('en_US', ''));
        $loadedLang = $this->jsonProvider->getLoadedLang();

        // then
        $this->assertNotNull($dict);
        $this->assertCount(2, $dict);
        $this->assertEquals('en_US Hello World!', $dict['Some']['HELLOWORLD']);
        $this->assertEquals('en_US', $loadedLang);
    }

    /**
     * @test
     * Try to load 'fr' file.
     */
    public function shouldDontLoadNonExistingFile(): void
    {
        // given

        // when
        $dict = $this->jsonProvider->load(array('fr', ''));
        $loadedLang = $this->jsonProvider->getLoadedLang();

        // then
        $this->assertNull($dict);
        $this->assertNull($loadedLang);
    }

    /**
     * @test
     * Try to load 'fr' file, using default lang 'en'
     */
    public function shouldLoadFromDefaultLangFile(): void
    {
        // given

        // when
        $dict = $this->jsonProvider->load(array('fr', 'en'));
        $loadedLang = $this->jsonProvider->getLoadedLang();

        // then
        $this->assertNotNull($dict);
        $this->assertCount(2, $dict);
        $this->assertEquals('en Hello World!', $dict['Some']['HELLOWORLD']);
        $this->assertEquals('en', $loadedLang);
    }

    /**
     * @test
     *
     *
     */
    public function shouldThrowExceptionWhenNoFileArePresentInThePath(): void
    {
        $this->expectExceptionMessage(CantLoadDictionaryException::NO_DICTIONARY_FILES);
        $this->expectException(CantLoadDictionaryException::class);
        // given
        $providerMock = $this->getMockBuilder(JsonProvider::class)
            ->onlyMethods(['listLangFiles'])
            ->getMock();
        // stub
        $providerMock->expects($this->once())->method('listLangFiles')->willReturn(array());

        // when
        $providerMock->load(['fr', 'en']);
    }

    /**
     * @test
     */
    public function shouldNotGetLoadedLangCodeBeforeLoading(): void
    {
        // when
        $loadedLang = $this->jsonProvider->getLoadedLang();

        // then
        $this->assertNull($loadedLang);
    }
}
