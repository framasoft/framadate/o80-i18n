<?php

namespace o80\i18n;

use o80\I18NTestCase;

class IniProviderUnitTest extends I18NTestCase
{
    /**
     * @var IniProvider
     */
    private $iniProvider;

    protected function setUp(): void
    {
        $this->iniProvider = new IniProvider();
        $this->iniProvider->setLangsPath($this->getTestResourcePath('langs'));
    }

    /**
     * @test
     * 'en' will match to 'en'
     */
    public function shouldLoadExactFileTranslation_en()
    {
        // given

        // when
        $dict = $this->iniProvider->load(array('en', ''));
        $loadedLang = $this->iniProvider->getLoadedLang();

        // then
        $this->assertNotNull($dict);
        $this->assertCount(1, $dict);
        $this->assertEquals('en Hello World!', $dict['HELLOWORLD']);
        $this->assertEquals('en', $loadedLang);
    }

    /**
     * @test
     * 'en_GB' will match to 'en'
     */
    public function shouldLoadMatchingFileTranslation_enGB(): void
    {
        // given

        // when
        $dict = $this->iniProvider->load(array('en_GB', ''));
        $loadedLang = $this->iniProvider->getLoadedLang();

        // then
        $this->assertNotNull($dict);
        $this->assertCount(1, $dict);
        $this->assertEquals('en Hello World!', $dict['HELLOWORLD']);
        $this->assertEquals('en', $loadedLang);
    }

    /**
     * @test
     * 'en_US' will match to 'en_US' instead of 'en'
     */
    public function shouldLoadExactFileTranslation_en_US(): void
    {
        // given

        // when
        $dict = $this->iniProvider->load(array('en_US', ''));
        $loadedLang = $this->iniProvider->getLoadedLang();

        // then
        $this->assertNotNull($dict);
        $this->assertCount(1, $dict);
        $this->assertEquals('en_US Hello World!', $dict['HELLOWORLD']);
        $this->assertEquals('en_US', $loadedLang);
    }

    /**
     * @test
     * Try to load 'fr' file.
     */
    public function shouldDontLoadNonExistingFile(): void
    {
        // given

        // when
        $dict = $this->iniProvider->load(['fr', '']);
        $loadedLang = $this->iniProvider->getLoadedLang();

        // then
        $this->assertNull($dict);
        $this->assertNull($loadedLang);
    }

    /**
     * @test
     * Try to load 'fr' file, using default lang 'en'
     */
    public function shouldLoadFromDefaultLangFile(): void
    {
        // given

        // when
        $dict = $this->iniProvider->load(array('fr', 'en'));
        $loadedLang = $this->iniProvider->getLoadedLang();

        // then
        $this->assertNotNull($dict);
        $this->assertCount(1, $dict);
        $this->assertEquals('en Hello World!', $dict['HELLOWORLD']);
        $this->assertEquals('en', $loadedLang);
    }

    /**
     * @test
     */
    public function shouldThrowExceptionWhenNoFileArePresentInThePath(): void
    {
        $this->expectExceptionMessage(CantLoadDictionaryException::NO_DICTIONARY_FILES);
        $this->expectException(CantLoadDictionaryException::class);
        // given
        $providerMock = $this->getMockBuilder(IniProvider::class)
            ->onlyMethods(['listLangFiles'])
            ->getMock();

        // stub
        $providerMock->expects($this->once())->method('listLangFiles')->willReturn([]);

        // when
        $providerMock->load(['fr', 'en']);
    }

    /**
     * @test
     */
    public function shouldNotGetLoadedLangCodeBeforeLoading(): void
    {
        // given
        $provider = new IniProvider();

        // when
        $loadedLang = $provider->getLoadedLang();

        // then
        $this->assertNull($loadedLang);
    }
}
